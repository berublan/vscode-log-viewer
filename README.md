# vscode-log-viewer README

Log file viewer.

## Features

-   Monitor files based on glob pattern ([picomatch](https://github.com/micromatch/picomatch))
    -   Absolute or relative to workspace
    -   Supports windows UNC paths
-   Clear log view (without modifying the file)
-   Automatically follow and unfollow tail based on scroll position
-   Support for large log files (only loads last 64KB)
-   Log highlighting (log4net)
-   A large number of supported encodings ([iconv-lite](https://github.com/ashtuchkin/iconv-lite))

## Configuration

-   `logViewer.watch`: Array of watches. A watch is either

    -   A glob pattern.
    -   An object defining a watch:
        -   `title`: Text to use in the explorer view and for the tab name.
        -   `pattern` (required): Glob pattern or array of glob patterns.
        -   `workspaceName`: When in a multi-root workspace, which workspace should be used to resolve relative patterns.
        -   `options`: Override options for this pattern. See available options below.
    -   An object defining a group:
        -   `groupName`: Name of the group.
        -   `watches`: Watches in this group.
    -   Patterns support some vscode variables:
        -   `${userHome}` - the path of the user's home folder
        -   `${workspaceFolder}` - the path of the folder opened in VS Code
        -   `${workspaceFolderBasename}` - the name of the folder opened in VS Code without any slashes (/)
        -   `${env:*}` - environment variables

-   `logViewer.options`

    -   `fileCheckInterval` (default=500): Interval in ms to check for changes in current file.
    -   `fileListInterval` (default=2000): Interval in ms to search for new files.
    -   `ignorePattern` (default="(node_modules|.git)"): This pattern is matched against each segment in the path. Each directory and the file.
    -   `encoding` (default="utf8"): Encoding to use when reading the files. (https://github.com/ashtuchkin/iconv-lite/wiki/Supported-Encodings).

-   `logViewer.windows.allowBackslashAsPathSeparator` (default=true): Allow to use \"\\\" (as well as \"/\") as a path separator on windows. Won't be able to escape certain pattern characters when enabled.

-   `logViewer.showStatusBarItemOnChange` (default=false): Show an item in the status bar when a watched pattern changes, to quickly access it.

-   `logViewer.chunkSizeKb` (default=64): Chunk size in kilobytes used to calculate the size of the last chunk to load in the viewer. The loaded last chunk will always be less than 2 \* chunkSizeKb.

-   `logViewer.logLevel` (default="error"): Logging level.

-   `logViewer.followTailMode` ("auto" or "manual, default="auto"): Follow tail behaviour.

    `auto` to start following tail when the end of the log is scrolled into the viewport, and stop following tail when it's scrolled out of the viewport.

    `manual` to only start and stop following tail explicitly.

## Example

```json
{
    "logViewer.watch": [
        {
            "title": "Demo App",
            "pattern": "/home/berni/Documentos/src/logger-sample/Logs/*.log"
        },
        {
            "title": "/var/logs",
            "pattern": "/var/log/**/*log"
        },
        {
            "title": "npm logs",
            "pattern": "/home/berni/.npm/_logs/*.log"
        }
    ]
}
```

![Screenshot](https://gitlab.com/berublan/vscode-log-viewer/raw/master/images/screenshot1.png)

![Screenshot](https://gitlab.com/berublan/vscode-log-viewer/raw/master/images/screenshot2.png)

## FAQ

### ¿How to configure coloring/syntax highlighting?

The coloring is provided by VSCode based on the extension of the pattern.

If the pattern doesn't include an extension, the syntax highlighting for `*.log-viewer` files that is included in this extension will be used.

Examples:

-   `/app/logs/*.xml`: will use syntax highlighting for `*.xml`
-   `/app/logs/*.log`: will use syntax highlighting for `*.log`
-   `/app/logs/*`: will use syntax highlighting for `*.log-viewer`

To configure syntax highlighting rules for `*.log` you can use another extesions like [Log File Highlighter](https://marketplace.visualstudio.com/items?itemName=emilast.LogFileHighlighter).

To make patterns without extension use same highlighting as `*.log` files, you can add this to your user settings:

```json
"files.associations": {
    "*.log-viewer": "log"
},
```
