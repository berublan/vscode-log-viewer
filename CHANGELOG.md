# Change Log

## [0.14.1]

-   Fix variable replacement on windows when `allowBackslashAsPathSeparator` is `false`

## [0.14.0]

-   Switch from `vscode.TextDocumentContentProvider` to `vscode.FileSystemProvider`
    -   avoid flickers in some cases ([#33](https://gitlab.com/berublan/vscode-log-viewer/-/issues/33))
    -   we now take advantage of vscode's encoding detection
-   Improve file search performance by using `readdir` overload that returns file type, thus avoiding a lot of `stat` calls
-   Allow multiple patterns watch entry
-   Save and restore groups expansion state
-   Resolve some vscode variables that make sense in patterns:
    -   `${userHome}` - the path of the user's home folder
    -   `${workspaceFolder}` - the path of the folder opened in VS Code
    -   `${workspaceFolderBasename}` - the name of the folder opened in VS Code without any slashes (/)
    -   `${env:*}` - environment variables

## [0.12.2]

-   allow arbitrarily nested watch groups, without validation and intellisense past the first level

## [0.12.1]

-   remove log syntax highlighting from extension because vscode already has one and it interferes with [Log File Highlighter](https://github.com/emilast/vscode-logfile-highlighter)
-   add `logViewer.followTailMode` to be able to disable automatic follow tail behaviour

## [0.11.1]

-   do not stop all watches when config changes, now only changed or removed watches are stopped
-   add more extension logs, and command to show them

## [0.11.0]

-   use [iconv-lite](https://github.com/ashtuchkin/iconv-lite) to support more encodings

## [0.10.3]

-   implement [#18](https://gitlab.com/berublan/vscode-log-viewer/issues/18) (Follow Tail / Don't Follow Tail icon is confusing)
    -   Now the text shows "Follow Tail: On/Off"
-   use codeicons instead of custom icons
-   dependency updates

## [0.10.2]

-   fix [#13](https://gitlab.com/berublan/vscode-log-viewer/issues/13) (`globalThis` is not defined in older versions of vscode)
-   use [`homedir`](https://nodejs.org/docs/latest-v12.x/api/os.html#os_os_homedir) to get user home directory

## [0.10.1]

-   fix for issue with `util.promisify(fs.read)` [#12](https://gitlab.com/berublan/vscode-log-viewer/issues/12)
-   fix follow tail in side-by-side mode [#11](https://gitlab.com/berublan/vscode-log-viewer/issues/11)

## [0.10.0]

-   added `logViewer.logLevel` option
-   replaced micromatch with picomatch, reducing runtime dependencies to just one

## [0.9.0]

-   watches grouping
-   fix last chunk load
-   configurable chunk size
-   extension bundling for faster loading

## [0.8.5]

-   fix clear command

## [0.8.4]

-   support ~ and $HOME at the begining of pattern

## [0.8.3]

-   improve follow tail
-   prevent new content form being selected after jumping at the end of the document

## [0.8.2]

-   fix clear/reset

## [0.8.0]

-   add option to notify on change in the status bar

## [0.7.0]

-   add encoding option

## [0.6.4]

-   fix relative patterns on windows

## [0.6.3]

-   Fix matching static file patterns

## [0.6.2]

-   Do not overwrite vscode log highlighting

## [0.6.1]

-   Fix bug introduced in 0.6.0 that broke patterns to specific files

## [0.6.0]

-   Handle escaped characters in patterns
-   Add `logViewer.windows.allowBackslashAsPathSeparator` option to be able to escape characters in windows, e.g.: `"C:/Program Files \\(x86\\)/MyApp/(server|client)/*.log"`

## [0.5.1]

-   Option to select workspace in multi-root workspaces

## [0.5.0]

-   UI redesign, adds a new section in the activity bar

## [0.4.2]

-   Fix handling "/" as a path separator in patterns in windows

## [0.4.0]

-   Automatically follow and unfollow tail based on scroll position

    Requires VS Code 1.22

## [0.3.1]

-   Small performance improvements
-   Denpendency updates

## [0.3.0]

-   Relative paths support

## [0.2.1]

-   Open current file from status bar
-   Bugfixes

## [0.2.0]

-   Replace chokidar with custom implementation better suited for this use case. Improved perfomance with:
    -   Patterns that match a lot of files
    -   Files over the network
-   Ability to set options per watch

## [0.1.3]

-   Fix oudated file info in status bar

## [0.1.2]

-   Initial release
