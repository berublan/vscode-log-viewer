#!/usr/bin/env bash

set -euxo pipefail

rm -f ./*.vsix
pnpm install --frozen-lockfile
pnpm run package

code --install-extension vscode-log-viewer-*.vsix --force