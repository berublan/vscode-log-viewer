import { runTests } from "@vscode/test-electron";
import { promises as fs } from "fs";
import * as path from "path";

// The folder containing the Extension Manifest package.json
// Passed to `--extensionDevelopmentPath`
const extensionDevelopmentPath = path.resolve(__dirname, "../../");

async function getVscodeVersion() {
    const vscodeVersion = process.argv[2] || "stable";
    if (vscodeVersion !== "minimum") {
        return vscodeVersion;
    }
    const packageJson = JSON.parse(
        await fs.readFile(path.join(extensionDevelopmentPath, "package.json"), { encoding: "utf8" }),
    ) as { engines: { vscode: string } };
    return packageJson.engines.vscode.replace(/^\^/, "");
}

async function main() {
    try {
        // The path to the extension test script
        // Passed to --extensionTestsPath
        const extensionTestsPath = path.resolve(__dirname, "./index");

        const testWorkspace = path.resolve(__dirname, "../../test/workspace");

        const vscodeVersion = await getVscodeVersion();

        // Download VS Code, unzip it and run the integration test
        await runTests({
            extensionDevelopmentPath,
            extensionTestsPath,
            launchArgs: [
                testWorkspace,
                "--disable-extensions", // for running locally
                "--no-sandbox", // without this it doesn't work in docker
            ],
            version: vscodeVersion,
            extensionTestsEnv: {
                TEST_FILTER: process.env["TEST_FILTER"],
            },
        });
    } catch (err) {
        // tslint:disable-next-line: no-console
        console.error("Failed to run tests");
        process.exit(1);
    }
}

void main();
