import * as path from "path";
import Mocha = require("mocha");
import fg = require("fast-glob");

export async function run(): Promise<void> {
    // Create the mocha test
    const mocha = new Mocha({
        ui: "tdd",
        color: true,
        grep: process.env["TEST_FILTER"],
    });

    const testsRoot = path.resolve(__dirname);

    const files = await fg("**/**.test.js", { cwd: testsRoot });
    for (const file of files) {
        mocha.addFile(path.resolve(testsRoot, file));
    }

    return new Promise((res, rej) => {
        mocha.run(failures => {
            if (failures > 0) {
                rej(new Error(`${failures} tests failed.`));
            } else {
                res();
            }
        });
    });
}
