import * as fs from "fs/promises";
import { encode } from "iconv-lite";
import * as path from "path";

const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 \t" +
    "~€‚ƒ„…†‡ˆ‰Š‹ŒŽ‘’“”•–—˜™š›œžŸ¡¢£¤¥¦§¨©ª«¬®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ";

export function randomString() {
    const len = 20 + Math.round(Math.random() * 100);
    const charCodes = new Array<number>(len);
    for (let i = 0; i < charCodes.length; i++) {
        charCodes[i] = characters.charCodeAt(Math.floor(Math.random() * characters.length));
    }
    return String.fromCharCode(...charCodes);
}

function randomFileName() {
    const str = Math.random().toString(16);
    if (str.length === 1) {
        // in case of 0 or 1
        return str;
    }
    return str.substring(2);
}

async function createSomeFilesAndDirectories(
    rootDir: string,
    filePrefix: string,
): Promise<FilesAndDirectories> {
    const step = async (dir: string, num: number, accDirs: string[], accFiles: string[]) => {
        // folders
        for (let i = 0; i < num; i++) {
            const subDir = path.join(dir, randomFileName());
            await fs.mkdir(subDir);
            accDirs.push(subDir);
            void step(subDir, num - 1, accDirs, accFiles);
        }

        // one file per level
        const file = path.join(dir, `${filePrefix}-${randomFileName()}.log`);
        accFiles.push(file);
    };
    const files: string[] = [];
    const directories: string[] = [];
    await step(rootDir, 3, directories, files);
    return {
        files,
        directories,
    };
}

export function elementAtRandom<T>(xs: readonly T[]): T {
    return xs[Math.floor(Math.random() * xs.length)];
}

export interface FilesAndDirectories {
    readonly files: readonly string[];
    readonly directories: readonly string[];
}

export interface TestCase {
    readonly title: string;
    readonly fsSetup: (baseDir: string) => Promise<FilesAndDirectories>;
    readonly logStep: (filesAndDirs: FilesAndDirectories) => Promise<{
        chunk: string;
        file: string;
    }>;
}

export const simpleTestCase: TestCase = {
    title: "simple",
    fsSetup: baseDir => createSomeFilesAndDirectories(baseDir, "simple"),
    logStep: async filesAndDirs => {
        const someFile = elementAtRandom(filesAndDirs.files);
        const lastAppended = randomString() + "\n";
        await fs.appendFile(someFile, lastAppended);
        return {
            chunk: lastAppended,
            file: someFile,
        };
    },
};

export const encodingTestCase: TestCase = {
    title: "encoding",
    fsSetup: baseDir => createSomeFilesAndDirectories(baseDir, "encoding"),
    logStep: async filesAndDirs => {
        const someFile = elementAtRandom(filesAndDirs.files);
        const lastAppended = randomString() + "\n";
        const encoded = encode(lastAppended, "win1252");
        await fs.appendFile(someFile, encoded);
        return {
            chunk: lastAppended,
            file: someFile,
        };
    },
};

export const multipatternTestCase: TestCase = {
    title: "multipattern",
    fsSetup: async baseDir => {
        const aDir = path.join(baseDir, "a");
        await fs.mkdir(aDir);
        const bDir = path.join(baseDir, "b");
        await fs.mkdir(bDir);
        const aFilesAndDirs = await createSomeFilesAndDirectories(aDir, "multipattern");
        const bFilesAndDirs = await createSomeFilesAndDirectories(bDir, "multipattern");
        return {
            files: aFilesAndDirs.files.concat(bFilesAndDirs.files),
            directories: aFilesAndDirs.directories.concat(bFilesAndDirs.directories),
        };
    },
    logStep: async filesAndDirs => {
        const someFile = elementAtRandom(filesAndDirs.files);
        const lastAppended = randomString() + "\n";
        await fs.appendFile(someFile, lastAppended);
        return {
            chunk: lastAppended,
            file: someFile,
        };
    },
};

export const testCases: readonly TestCase[] = [simpleTestCase, encodingTestCase, multipatternTestCase];

export class TestLogger {
    constructor(
        public readonly title: string,
        public readonly filesAndDirs: FilesAndDirectories,
        private readonly _logStep: TestCase["logStep"],
    ) {}
    logStep() {
        return this._logStep(this.filesAndDirs);
    }
    async removeFilesAndDirs() {
        for (const f of this.filesAndDirs.files) {
            await fs.rm(f, { force: true });
        }
        for (const d of this.filesAndDirs.directories) {
            await fs.rm(d, { recursive: true, force: true });
        }
    }
}

export async function createTestLogger(
    baseDir: string,
    { title, fsSetup, logStep }: TestCase,
): Promise<TestLogger> {
    const filesAndDirs = await fsSetup(baseDir);
    return new TestLogger(title, filesAndDirs, logStep);
}
