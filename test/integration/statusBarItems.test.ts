import assert = require("assert");
import * as vscode from "vscode";
import * as fs from "fs/promises";
import * as path from "path";
import { delay } from "../../src/common/util";
import { unwatchCmd } from "../../src/vscode/logExplorer";
import {
    executeCommand,
    findDocumentByTitle,
    getWaitIntervalMsAndSetuptTimeout,
    setupExtension,
    setupLogsBaseDir,
    startWatchByTitle,
} from "./integrationUtils";
import {
    createTestLogger,
    multipatternTestCase,
    randomString,
    simpleTestCase,
    type TestCase,
    type TestLogger,
} from "./testCases";

const singleFileTestCase: TestCase = {
    title: "simple",
    fsSetup: baseDir => Promise.resolve({ directories: [], files: [path.join(baseDir, "simple-0.log")] }),
    logStep: async filesAndDirs => {
        const someFile = filesAndDirs.files[0];
        const lastAppended = randomString() + "\n";
        await fs.appendFile(someFile, lastAppended);
        return {
            chunk: lastAppended,
            file: someFile,
        };
    },
};

suite("StatusBarItems", function () {
    const waitIntervalMs = getWaitIntervalMsAndSetuptTimeout(this);

    let document: vscode.TextDocument | undefined;
    let logger: TestLogger | undefined;

    suiteTeardown(async () => {
        if (logger) {
            await logger.removeFilesAndDirs();
        }
        if (document) {
            await vscode.commands.executeCommand(unwatchCmd, document);
        }
    });

    test("Clear and Reset", async () => {
        const logsBaseDir = await setupLogsBaseDir();

        const extensionTestHandles = await setupExtension();
        assert(extensionTestHandles.clear.command);
        assert(extensionTestHandles.reset.command);

        await startWatchByTitle(extensionTestHandles, singleFileTestCase.title);

        document = findDocumentByTitle(singleFileTestCase.title);
        teardown(`${singleFileTestCase.title}: unwatch`, () =>
            vscode.commands.executeCommand(unwatchCmd, document),
        );
        await vscode.window.showTextDocument(document, { preview: false });

        logger = await createTestLogger(logsBaseDir, singleFileTestCase);

        let initialText = "";
        for (let i = 0; i < 10; i++) {
            const { chunk } = await logger.logStep();
            initialText += chunk;
        }
        await delay(waitIntervalMs);

        assert.strictEqual(document.getText(), initialText, "initial text");

        // clear
        await executeCommand(extensionTestHandles.clear.command);
        await delay(waitIntervalMs);
        assert.strictEqual(document.getText(), "", "after clear");

        // new text is loaded
        let newText = "";
        for (let i = 0; i < 3; i++) {
            const { chunk } = await logger.logStep();
            newText += chunk;
        }
        await delay(waitIntervalMs);
        assert.strictEqual(document.getText(), newText, "new text");

        // restore
        await executeCommand(extensionTestHandles.reset.command);
        await delay(waitIntervalMs);
        assert.strictEqual(document.getText(), initialText + newText, "restore");
    });

    test("Last Changes", async () => {
        const logsBaseDir = await setupLogsBaseDir();

        const extensionTestHandles = await setupExtension();

        const testCase1 = simpleTestCase;
        const testCase2 = multipatternTestCase;

        // loggers
        const logger1 = await createTestLogger(logsBaseDir, testCase1);
        teardown(`${logger1.title}: remove logger files`, () => logger1.removeFilesAndDirs());

        const logger2 = await createTestLogger(logsBaseDir, testCase2);
        teardown(`${logger2.title}: remove logger files`, () => logger2.removeFilesAndDirs());

        // start watches
        await startWatchByTitle(extensionTestHandles, testCase1.title);
        const document1 = findDocumentByTitle(testCase1.title);
        teardown(`${testCase1.title}: unwatch`, () => vscode.commands.executeCommand(unwatchCmd, document1));

        await startWatchByTitle(extensionTestHandles, testCase2.title);
        const document2 = findDocumentByTitle(testCase2.title);
        teardown(`${testCase2.title}: unwatch`, () => vscode.commands.executeCommand(unwatchCmd, document2));

        // show 1 with changes in both, lastChange should be for 2
        await vscode.window.showTextDocument(document1, { preview: false });
        await logger2.logStep();
        await logger1.logStep();
        await delay(waitIntervalMs);
        assert(
            extensionTestHandles.lastChange.text.includes(testCase2.title),
            `expected lastChange.text to include "${testCase2.title}" but it's "${extensionTestHandles.lastChange.text}"`,
        );

        // show 2 with changes in both, lastChange should be for 1
        await vscode.window.showTextDocument(document2, { preview: false });
        await logger1.logStep();
        await logger2.logStep();
        await delay(waitIntervalMs);
        assert(
            extensionTestHandles.lastChange.text.includes(testCase1.title),
            `expected lastChange.text to include "${testCase1.title}" but it's "${extensionTestHandles.lastChange.text}"`,
        );
    });
});
