import assert = require("assert");
import * as fs from "fs/promises";
import * as path from "path";
import * as vscode from "vscode";
import type { ExtensionTestHandles } from "../../src/extension";
import * as inspector from "inspector";
import { fromLogUri } from "../../src/vscode/logUri";

export async function setupLogsBaseDir(): Promise<string> {
    const workspaceRoot = vscode.workspace.workspaceFolders?.[0];
    assert(workspaceRoot);
    const logsBaseDir = path.resolve(workspaceRoot.uri.fsPath, "log");
    await fs.rm(logsBaseDir, { recursive: true, force: true });
    await fs.mkdir(logsBaseDir, { recursive: true });
    return logsBaseDir;
}

let _extensionTestHandles: ExtensionTestHandles | undefined;
export async function setupExtension(): Promise<ExtensionTestHandles> {
    if (_extensionTestHandles == null) {
        const extension = vscode.extensions.getExtension("berublan.vscode-log-viewer");
        assert(extension);
        _extensionTestHandles = (await extension.activate()) as unknown as ExtensionTestHandles;
    }
    return _extensionTestHandles;
}

export function getWaitIntervalMsAndSetuptTimeout(mochaCtx: Mocha.Context | Mocha.Suite): number {
    let waitIntervalMs: number;
    if (process.env["CI"]) {
        waitIntervalMs = 1000;
        mochaCtx.timeout(waitIntervalMs * 20);
    } else {
        waitIntervalMs = 200;
        if (inspector.url()) {
            mochaCtx.timeout(0);
        } else {
            mochaCtx.timeout(waitIntervalMs * 25);
        }
    }
    return waitIntervalMs;
}

export async function findItemByTitle({ treeDataProvider }: ExtensionTestHandles, title: string) {
    const item = (await treeDataProvider.getChildren())?.find(x => x.kind === "watch" && x.title === title);
    assert(item, `item not found for "${title}"`);
    return item;
}

export function executeCommand(cmd: vscode.Command | string) {
    if (typeof cmd === "string") {
        return vscode.commands.executeCommand(cmd);
    }
    if (cmd.arguments) {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
        return vscode.commands.executeCommand(cmd.command, ...cmd.arguments);
    } else {
        return vscode.commands.executeCommand(cmd.command);
    }
}

export async function startWatchByTitle(eths: ExtensionTestHandles, title: string): Promise<void> {
    const item = await findItemByTitle(eths, title);

    await eths.treeView.reveal(item);

    const command = (await eths.treeDataProvider.getTreeItem(item)).command;
    assert(command, `no command for "${title}"`);
    assert(command.arguments, `no command arguments for "${title}"`);

    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    await vscode.commands.executeCommand(command.command, ...command.arguments);

    const treeItem = await eths.treeDataProvider.getTreeItem(item);
    assert(treeItem.contextValue == "watching", `treeItem.contextValue for "${title}" not "watching"`);
}

export function findDocumentByTitle(title: string): vscode.TextDocument {
    const document = vscode.workspace.textDocuments.find(doc => fromLogUri(doc.uri).title === title);
    assert(document, `no document with title "${title}"`);
    return document;
}
