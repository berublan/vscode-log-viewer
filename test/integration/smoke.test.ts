import assert = require("assert");
import * as path from "path";
import * as vscode from "vscode";
import { delay } from "../../src/common/util";
import type { ExtensionTestHandles } from "../../src/extension";
import { unwatchCmd } from "../../src/vscode/logExplorer";
import {
    findDocumentByTitle,
    findItemByTitle,
    getWaitIntervalMsAndSetuptTimeout,
    setupExtension,
    setupLogsBaseDir,
    startWatchByTitle,
} from "./integrationUtils";
import { createTestLogger, elementAtRandom, TestCase, testCases } from "./testCases";

async function runTestCase(
    eths: ExtensionTestHandles,
    testCase: TestCase,
    logsBaseDir: string,
    waitIntervalMs: number,
) {
    const document = findDocumentByTitle(testCase.title);
    teardown(`${testCase.title}: unwatch`, () => vscode.commands.executeCommand(unwatchCmd, document));
    assert.strictEqual(document.getText(), "no matching file found");

    const logger = await createTestLogger(logsBaseDir, testCase);
    teardown(`${logger.title}: remove logger files`, () => logger.removeFilesAndDirs());

    for (let i = 0; i < 10; i++) {
        const { chunk: expectedText, file: expectedFile } = await logger.logStep();
        await delay(waitIntervalMs);
        const documentText = document.getText();
        assert(
            documentText.endsWith(expectedText),
            `"${documentText}" didn't end with "${expectedText}" for "${testCase.title}"`,
        );
        if (document === vscode.window.activeTextEditor?.document) {
            const expectedFileName = path.basename(expectedFile);
            assert(
                eths.watchingInfo.text.includes(expectedFileName),
                `expected "${eths.watchingInfo.text}" to include "${expectedFileName}"`,
            );
        }
    }
    await logger.removeFilesAndDirs();

    await delay(waitIntervalMs);

    assert.strictEqual(document.getText(), "no matching file found");

    await vscode.commands.executeCommand(unwatchCmd, document);

    const item = await findItemByTitle(eths, testCase.title);
    const treeItem = await eths.treeDataProvider.getTreeItem(item);
    assert.strictEqual(treeItem.contextValue, undefined);
}

test("Smoke test", async function () {
    const waitIntervalMs = getWaitIntervalMsAndSetuptTimeout(this);

    const logsBaseDir = await setupLogsBaseDir();
    const extensionTestHandles = await setupExtension();

    for (const testCase of testCases) {
        await startWatchByTitle(extensionTestHandles, testCase.title);
    }

    let testCasesDone = false;
    // change the active document randomly to verify it doesn't affect the watching logic
    const changeActiveEditorPromise = (async () => {
        while (!testCasesDone) {
            await delay(waitIntervalMs);
            if (vscode.window.visibleTextEditors.length) {
                const randomTextDocument = elementAtRandom(vscode.workspace.textDocuments);
                await vscode.window.showTextDocument(randomTextDocument, { preview: false });
            }
        }
    })();

    await Promise.all(
        testCases.map(testCase => runTestCase(extensionTestHandles, testCase, logsBaseDir, waitIntervalMs)),
    );

    testCasesDone = true;
    await changeActiveEditorPromise;
});
