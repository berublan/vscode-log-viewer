import { strict as assert } from "assert";
import { resolveVariables, VariableResolveContext } from "../../src/common/config";
import { setPathImpl } from "../../src/common/util";
import path from "path";

suite("resolveVaraibles", () => {
    test("$HOME", () => {
        const ctx: VariableResolveContext = {
            env: {},
            home: "/home/test_home",
            workspaceFolder: undefined,
            allowBackslashAsPathSeparator: false,
        };
        assert.equal(resolveVariables("$HOME/foo/", ctx), "/home/test_home/foo/");
        assert.equal(resolveVariables("~/foo/", ctx), "/home/test_home/foo/");
        assert.equal(resolveVariables("${userHome}/foo/", ctx), "/home/test_home/foo/");
    });

    test("not $HOME", () => {
        const ctx: VariableResolveContext = {
            env: {},
            home: "/home/test_home",
            workspaceFolder: undefined,
            allowBackslashAsPathSeparator: false,
        };
        assert.equal(resolveVariables("/foo/$HOME/bar/", ctx), "/foo/$HOME/bar/");
        assert.equal(resolveVariables("/foo/~/bar/", ctx), "/foo/~/bar/");
        assert.equal(resolveVariables("$userHome/foo/", ctx), "$userHome/foo/");
    });

    test("env", () => {
        const ctx: VariableResolveContext = {
            env: { FOO: "foo" },
            home: "/home/test_home",
            workspaceFolder: undefined,
            allowBackslashAsPathSeparator: false,
        };
        assert.equal(resolveVariables("/${env:FOO}/bar/", ctx), "/foo/bar/");
        assert.equal(resolveVariables("/${env:foo}/bar/", ctx), "/${env:foo}/bar/");
        assert.equal(resolveVariables("/${env:BAZ}/bar/", ctx), "/${env:BAZ}/bar/");
    });

    test("workspaceFolder", () => {
        const ctx: VariableResolveContext = {
            env: {},
            home: "/home/test_home",
            workspaceFolder: "/workspace/foo",
            allowBackslashAsPathSeparator: false,
        };
        assert.equal(resolveVariables("${workspaceFolder}/bar/", ctx), "/workspace/foo/bar/");
        assert.equal(resolveVariables("/logs/${workspaceFolderBasename}/*.log", ctx), "/logs/foo/*.log");
    });

    test("workspaceFolder missing", () => {
        const ctx: VariableResolveContext = {
            env: {},
            home: "/home/test_home",
            workspaceFolder: undefined,
            allowBackslashAsPathSeparator: false,
        };
        assert.equal(resolveVariables("${workspaceFolder}/bar/", ctx), "${workspaceFolder}/bar/");
        assert.equal(
            resolveVariables("/logs/${workspaceFolderBasename}/*.log", ctx),
            "/logs/${workspaceFolderBasename}/*.log",
        );
    });

    test("windows", () => {
        setPathImpl(path.win32);
        teardown(() => setPathImpl(undefined));

        const ctx: VariableResolveContext = {
            env: {},
            home: "C:\\Users\\test",
            workspaceFolder: "W:\\workspace\\foo",
            allowBackslashAsPathSeparator: false,
        };
        assert.equal(resolveVariables("$HOME/foo/", ctx), "C:/Users/test/foo/");
        assert.equal(resolveVariables("~/foo/", ctx), "C:/Users/test/foo/");
        assert.equal(resolveVariables("${userHome}/foo/", ctx), "C:/Users/test/foo/");
        assert.equal(resolveVariables("${workspaceFolder}/bar/", ctx), "W:/workspace/foo/bar/");
        assert.equal(resolveVariables("W:/logs/${workspaceFolderBasename}/*.log", ctx), "W:/logs/foo/*.log");
    });
});
