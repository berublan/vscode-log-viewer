import "../requireMock";

import * as fg from "fast-glob";
import * as os from "os";
import { performance } from "perf_hooks";
import { lsPattern } from "../../src/common/fsUtil";
import { toPathMatcher } from "../../src/common/mmUtil";

async function bench(fn: () => Promise<void>) {
    const nWarm = 3;
    for (let i = 0; i < nWarm; i++) {
        await fn();
    }
    const n = 5;
    const t0 = performance.now();
    for (let i = 0; i < n; i++) {
        await fn();
    }
    const t1 = performance.now();
    console.log(`${(t1 - t0) / n} ms/op`);
}

interface Args {
    impl?: string;
    pattern?: string;
}

function parseArgs(): Args {
    const res: Args = {};
    // skip node and script name
    const args = process.argv.slice(2);
    for (let i = 0; i < args.length; i++) {
        switch (args[i]) {
            case "--impl":
                res.impl = args[++i];
                break;
            case "--pattern":
                res.pattern = args[++i];
                break;
        }
    }
    return res;
}

async function main() {
    const baseFolder = `${os.homedir()}/Documents/src/solid`;

    const args = parseArgs();
    const pattern = args.pattern ?? "**/*.css";

    const res: string[] = [];

    if (!args.impl || "fs-glob".includes(args.impl)) {
        console.log("fs-glob");
        await bench(async () => {
            res.splice(0);
            const xs = await fg.default(`${baseFolder}/${pattern}`, { dot: true, stats: true });
            for (const x of xs) {
                res.push(x.path);
            }
        });
        console.log(res.length);
    }

    if (!args.impl || "lsPattern".includes(args.impl)) {
        console.log("lsPattern");
        await bench(async () => {
            res.splice(0);
            await lsPattern(
                toPathMatcher(pattern, { cwd: baseFolder }),
                f => {
                    res.push(f.fullPath);
                },
                e => console.error(e),
            );
        });
        console.log(res.length);
    }
}
void main();
