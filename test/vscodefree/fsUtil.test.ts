import assert = require("assert");
import fastGlob from "fast-glob";
import * as path from "path";
import type { FsWalkerSubscription } from "../../src/common/fsWalker";
import { getNodeModulesDir, myWalkerFun, testsInit } from "./testUtil";

testsInit();

async function assertTerminates<T>(promise: Promise<T>, timeout?: number, message?: string): Promise<T> {
    return new Promise<T>((res, rej) => {
        const timeoutHandle = setTimeout(() => {
            rej(message ?? "didn't complete in time");
        }, timeout ?? 250);
        promise
            .then(x => {
                clearTimeout(timeoutHandle);
                res(x);
            })
            .catch(e => {
                clearTimeout(timeoutHandle);
                rej(e);
            });
    });
}

suite("lsPattern", () => {
    test("terminates on invalid path", async () => {
        const noOpSub: FsWalkerSubscription = {
            onFile: fi => {
                assert.fail(fi.fullPath);
            },
            onError: () => {},
        };
        await assertTerminates(myWalkerFun("/notexisting/path").walk(noOpSub));
    });

    test("node_modules/**/*.d.ts", async () => {
        const pattern = path.join(getNodeModulesDir(), "**", "*.d.ts");

        // find expected files with fsGlob
        const expectedFiles: string[] = [];
        const fastGlobPromise = fastGlob(pattern, { dot: true }).then(xs => {
            expectedFiles.push(...xs);
        });

        // find files with lsPattern
        const foundFiles: string[] = [];
        const lsPatternPromise = myWalkerFun(pattern).walk({
            onFile: fi => {
                foundFiles.push(fi.fullPath);
            },
            onError: _err => {
                // assert.fail(err.toString()); TODO?
            },
        });

        await assertTerminates(fastGlobPromise, 10_000, "fastGlobPromise < 10_000ms");
        await assertTerminates(lsPatternPromise, 10_000, "lsPatternPromise < 10_000ms");

        assert(foundFiles.length);

        assert.deepStrictEqual(foundFiles.sort(), expectedFiles.sort());
    });
});
