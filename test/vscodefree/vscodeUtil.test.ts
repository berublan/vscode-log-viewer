import * as assert from "assert";
import type { Uri } from "vscode";
import { getWorkspaceDir } from "../../src/common/util";
import type { WorkspaceFolder } from "../../src/common/vscodeTypes";
import { testsInit } from "./testUtil";

testsInit();

suite("vscodeUtil", () => {
    const workspaceFoldersMock: WorkspaceFolder[] = [
        {
            index: 0,
            name: "main",
            uri: {
                fsPath: "/home/berni/main",
            } as Uri,
        },
        {
            index: 1,
            name: "alt",
            uri: {
                fsPath: "/home/berni/alt",
            } as Uri,
        },
    ];

    interface TestCase {
        workspaceName: string | undefined;
        expectedWorkspaceDir: string;
    }

    test("getWorkspaceDir with workspaces", () => {
        const testCases: TestCase[] = [
            {
                workspaceName: "unkown",
                expectedWorkspaceDir: "/home/berni/main",
            },
            {
                workspaceName: undefined,
                expectedWorkspaceDir: "/home/berni/main",
            },
            {
                workspaceName: "main",
                expectedWorkspaceDir: "/home/berni/main",
            },
            {
                workspaceName: "alt",
                expectedWorkspaceDir: "/home/berni/alt",
            },
        ];

        for (const testCase of testCases) {
            const actualWorkspaceDir = getWorkspaceDir(workspaceFoldersMock, testCase.workspaceName);
            assert.strictEqual(actualWorkspaceDir, testCase.expectedWorkspaceDir);
        }
    });

    test("getWorkspaceDir without workspaces", () => {
        assert.strictEqual(getWorkspaceDir(undefined, "main"), undefined);
    });
});
