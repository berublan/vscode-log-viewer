// tslint:disable: no-console
import * as path from "path";
import * as fs from "fs";
import assert = require("assert");

type Obj = Record<string, unknown>;

const baseDir = path.resolve(__dirname, "..", "..");

async function readJSON(filepath: string): Promise<unknown> {
    const text = await fs.promises.readFile(filepath, { encoding: "utf8" });
    return JSON.parse(text) as unknown;
}

async function writeJSON(filepath: string, obj: any): Promise<void> {
    const text = JSON.stringify(obj, undefined, "    ");
    await fs.promises.writeFile(filepath, text, { encoding: "utf8" });
}

async function main() {
    const schemaPath = path.resolve(baseDir, "log-viewer.schema.json");
    const packageJsonPath = path.resolve(baseDir, "package.json");

    const schema = new Schema((await readJSON(schemaPath)) as Obj);
    const configuration = schema.toConfig();

    const packageJson = (await readJSON(packageJsonPath)) as { contributes: { configuration: Obj } };
    packageJson.contributes.configuration = configuration;

    await writeJSON(packageJsonPath, packageJson);
}

function clone<T>(x: T): T {
    return JSON.parse(JSON.stringify(x)) as T;
}

function isObj(x: unknown): x is Obj {
    return x != null && typeof x === "object";
}

const MaxRecursion = 1;

type DepthByRef = Readonly<Record<string, number>>;
function increaseRefDepth(depthByRef: DepthByRef, ref: string): DepthByRef {
    const res = { ...depthByRef };
    res[ref] ??= 0;
    res[ref]++;
    return res;
}

const MaxRecursionReached = Symbol("MaxRecursionReached");

class Schema {
    constructor(private readonly schema: Obj) {}

    private getRef(ref: string) {
        const parts = ref.split("/");
        assert(parts[0], "#");
        let dest = this.schema;
        for (let i = 1; i < parts.length; i++) {
            dest = dest[parts[i]] as Obj;
        }
        return clone(dest);
    }

    private resolveArray(items: readonly unknown[], depthByRef: DepthByRef): unknown[] {
        const resolvedItems: unknown[] = [];
        for (const item of items) {
            const resolvedItem = this.resolveUnkown(item, depthByRef);
            if (resolvedItem !== MaxRecursionReached) {
                resolvedItems.push(resolvedItem);
            }
        }
        return resolvedItems;
    }

    private resolveObj(obj: Readonly<Obj>, depthByRef: DepthByRef): Obj | typeof MaxRecursionReached {
        if ("$ref" in obj) {
            const { $ref, ...rest } = obj as { $ref: string } & Obj;
            const refDepth = depthByRef[$ref] ?? 0;
            if (refDepth > MaxRecursion) {
                return MaxRecursionReached;
            }
            const def = {
                ...rest,
                ...this.getRef($ref),
            };
            return this.resolveObj(def, increaseRefDepth(depthByRef, $ref));
        } else {
            const resolvedObj: Obj = {};
            for (const k of Object.keys(obj)) {
                const resolvedProp = this.resolveUnkown(obj[k], depthByRef);
                if (resolvedProp !== MaxRecursionReached) {
                    resolvedObj[k] = resolvedProp;
                }
            }
            return resolvedObj;
        }
    }

    private resolveUnkown(type: unknown, depthByRef: DepthByRef) {
        if (Array.isArray(type)) {
            return this.resolveArray(type, depthByRef);
        } else if (isObj(type)) {
            return this.resolveObj(type, depthByRef);
        } else {
            return type;
        }
    }

    public resolve(type: unknown): unknown {
        return this.resolveUnkown(type, {});
    }
    public toConfig() {
        const config = clone(this.schema);
        const res = this.resolve(config) as Obj;
        delete res["definitions"];
        delete res["$schema"];
        return res;
    }
}

void main();
