// @ts-check
import esbuild from "esbuild";
import path from "node:path";
import fs from "node:fs/promises";
import { fileURLToPath } from "node:url";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const mode = /** @type {"development"|"production"|undefined} */ (process.env.NODE_ENV) ?? "development";
const watch = Boolean(process.env.WATCH);

const outdir = path.join(__dirname, "dist");

await fs.rm(outdir, { recursive: true, force: true });

/** @type {esbuild.Plugin[]} */
const plugins = [
    {
        name: "split-iconv-lite",
        setup(build) {
            build.onResolve({ filter: /^iconv-lite$/ }, args => {
                if (args.kind === "entry-point") {
                    return;
                }
                return {
                    path: "./iconv-lite",
                    external: true,
                };
            });
        },
    },
];
if (watch) {
    plugins.push({
        name: "vscode-problem-matcher-helper",
        setup: build => {
            build.onEnd(() => {
                // for problemMatcher, so that preLaunchTask works properly
                console.log("REBUILD");
                console.log("DONE");
            });
        },
    });
}

/** @type {esbuild.BuildOptions} */
const buildOptions = {
    absWorkingDir: __dirname,
    bundle: true,
    target: ["node14"],
    platform: "node",
    outdir,
    define: {
        "process.env.NODE_ENV": JSON.stringify(mode),
        DEVELOPMENT: JSON.stringify(mode === "development"),
    },
    plugins,
    sourcemap: mode === "development" ? "linked" : false,
    metafile: true,
    minify: mode === "production",
    logLevel: "info",
    entryPoints: {
        extension: "src/extension.ts",
        "iconv-lite": "iconv-lite",
    },
    external: ["vscode"],
};

if (watch) {
    const context = await esbuild.context(buildOptions);
    await context.watch();
} else {
    await esbuild.build(buildOptions);
}
