module.exports = {
    root: true,
    parser: "@typescript-eslint/parser",
    parserOptions: {
        tsconfigRootDir: __dirname,
        project: ["./tsconfig.json"],
    },
    plugins: ["@typescript-eslint"],
    extends: [
        "eslint:recommended",
        "plugin:@typescript-eslint/eslint-recommended",
        "plugin:@typescript-eslint/recommended",
        "plugin:@typescript-eslint/recommended-requiring-type-checking",
    ],
    rules: {
        "@typescript-eslint/interface-name-prefix": "off",
        "@typescript-eslint/explicit-function-return-type": "off",
        "@typescript-eslint/no-use-before-define": "off",
        "@typescript-eslint/no-explicit-any": "off",
        "@typescript-eslint/no-empty-function": "off",
        "no-unused-vars": "off",
        "@typescript-eslint/no-unused-vars": [
            "warn",
            {
                argsIgnorePattern: "^_",
            },
        ],
        "@typescript-eslint/ban-types": [
            "warn",
            {
                types: {
                    // un-ban a type that's banned by default
                    "{}": false,
                },
                extendDefaults: true,
            },
        ],
        "@typescript-eslint/require-await": "warn",
        "@typescript-eslint/no-unsafe-assignment": "warn",
        "@typescript-eslint/no-unsafe-call": "warn",
        "@typescript-eslint/no-unsafe-argument": "warn",
        "no-restricted-imports": [
            "error",
            {
                patterns: [
                    {
                        group: ["node:*"],
                        message: "not supported on node 14",
                    },
                ],
            },
        ],
    },
    overrides: [
        {
            files: ["src/**/*"],
            rules: {
                "no-restricted-imports": [
                    "error",
                    {
                        paths: [
                            {
                                name: "path",
                                message: "Use getPathImpl",
                            },
                            {
                                name: "node:path",
                                message: "Use getPathImpl",
                            },
                        ],
                        patterns: [
                            {
                                group: ["node:*"],
                                message: "not supported on node 14",
                            },
                        ],
                    },
                ],
            },
        },
    ],
};
