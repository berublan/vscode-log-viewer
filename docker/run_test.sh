#!/bin/bash
set -euxo pipefail

# this export is required
export DISPLAY=:99.0

if [ -z "$(pidof /usr/bin/Xvfb)" ]
then
    Xvfb -ac $DISPLAY &
fi

cd /mnt/src/

pnpm run clean
pnpm install --frozen-lockfile
pnpm add --global @vscode/vsce
pnpm run package
pnpm run build
pnpm run lint

pnpm run test "${1:-""}"
