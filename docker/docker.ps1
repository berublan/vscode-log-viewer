#!/usr/bin/pwsh
using namespace System.IO

param(
    [Parameter(ParameterSetName = "build", Mandatory = $true)]
    [switch]
    $build,
    [Parameter(ParameterSetName = "build")]
    [switch]
    $force,

    [Parameter(ParameterSetName = "push", Mandatory = $true)]
    [switch]
    $push,

    [Parameter(ParameterSetName = "run", Mandatory = $true)]
    [switch]
    $run,
    [Parameter(ParameterSetName = "run")]
    [switch]
    $clean
)

Push-Location $PSScriptRoot

$tag = "log-viewer-docker:latest"
$registry = "registry.gitlab.com/berublan/vscode-log-viewer"

if ($build) {
    $dockerArgs = @("build", "-t", $tag, ".")
    if ($force) {
        $dockerArgs += "--no-cache"
    }
    docker $dockerArgs
}

if ($push) {
    docker tag $tag $registry
    docker push $registry
}

if ($run) {

    $worspace_repo_dir = [Path]::Join($PSScriptRoot, "workspace")
    if ($clean) {
        Remove-Item -Recurse -Force -Confirm $worspace_repo_dir
    }

    $repo_dir = Join-Path $PSScriptRoot ".." -Resolve
    Push-Location $repo_dir
    git ls-files | ForEach-Object {
        $srcFi = [FileInfo]::new([Path]::Join($repo_dir, $_))
        if ($srcFi.Exists) {
            $dstFi = [FileInfo]::new([Path]::Join($worspace_repo_dir, $_))
            $dstFi.Directory.Create() > $null
            $srcFi.CopyTo($dstFi.FullName, $true) > $null
        }
    }
    Pop-Location

    # 1000 is my user id and the user id of the node user in the container
    docker run --rm --interactive --tty --volume "$($worspace_repo_dir):/mnt/src" `
        --user "1000:1000" $tag bash
}

Pop-Location