import type { Event, WorkspaceFolder } from "./vscodeTypes";
// eslint-disable-next-line no-restricted-imports
import * as path from "path";

export function assertNever(x: never): never {
    // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
    throw new Error(`${x} is not never`);
}

type Keys<T> = {
    [P in keyof T]: P;
};

export function keys<T extends {}>(x: T): Keys<T> {
    const res: { [x: string]: string } = {};
    for (const k of Object.keys(x)) {
        res[k] = k;
    }
    return res as any as Keys<T>;
}

export async function delay(ms: number): Promise<void> {
    return new Promise<void>(res => {
        setTimeout(res, ms);
    });
}

export class Deferred<T> {
    public readonly promise: Promise<T>;
    private _resolve!: (x: T) => void;
    private _reject!: (reason?: any) => void;

    public get resolve(): (x: T) => void {
        return this._resolve;
    }

    public get reject(): (reason?: any) => void {
        return this._reject;
    }

    constructor() {
        this.promise = new Promise((resolve, reject) => {
            this._reject = reject;
            this._resolve = resolve;
        });
    }
}

export function getWorkspaceDir(
    // pass as parameter for easier testing
    workspaceFolders: readonly WorkspaceFolder[] | undefined,
    workspaceName: string | undefined,
): string | undefined {
    if (workspaceFolders?.length) {
        let workspaceFolder = workspaceFolders[0];
        if (workspaceName) {
            const wf = workspaceFolders.find(x => x.name === workspaceName);
            if (wf) {
                workspaceFolder = wf;
            }
        }
        if (workspaceFolder) {
            return workspaceFolder.uri.fsPath;
        }
    }
    return;
}

export function mapEvent<T, R>(event: Event<T>, func: (x: T) => R): Event<R> {
    return (listener, thisArgs, disposables) => {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-return
        return event(x => listener(func(x)), thisArgs, disposables);
    };
}

let _devEnv = false;
export function setDevEnv(val: boolean): void {
    _devEnv = val;
}

export function isDevEnv(): boolean {
    return _devEnv;
}

export function patternDescription(pattern: string | string[]): string {
    if (Array.isArray(pattern)) {
        return pattern.join(",");
    } else {
        return pattern;
    }
}

let _pathImpl: path.PlatformPath | undefined;
export function getPathImpl(): path.PlatformPath {
    return _pathImpl ?? path;
}

export function setPathImpl(pathImpl: path.PlatformPath | undefined): void {
    _pathImpl = pathImpl;
}
