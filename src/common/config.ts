import { getPathImpl } from "./util";
import type { Event } from "./vscodeTypes";

export enum LogLevel {
    trace = 0,
    debug = 1,
    info = 2,
    warn = 3,
    error = 4,
}

export interface WatchGroup {
    kind: "group";
    readonly groupName: string;
    readonly watches: ReadonlyArray<WatchEntry>;
}

export interface Watch {
    kind: "watch";
    readonly id: number;
    readonly title: string | undefined;
    readonly pattern: string | string[];
    readonly workspaceName: string | undefined;
    readonly options: Partial<WatchOptions> | undefined;
}

export type WatchEntry = WatchGroup | Watch;

export interface WatchOptions {
    readonly fileCheckInterval: number;
    readonly fileListInterval: number;
    readonly ignorePattern: string;
    readonly encoding: string | undefined | null;
}

export interface ConfigTypeMap {
    options: Partial<WatchOptions>;
    windows: WindowsConfig;
    showStatusBarItemOnChange: boolean;
    chunkSizeKb: number;
    logLevel: keyof typeof LogLevel;
    followTailMode: "auto" | "manual";
}

interface WindowsConfig {
    allowBackslashAsPathSeparator: boolean;
}

export interface IConfigService {
    get<K extends keyof ConfigTypeMap>(key: K): ConfigTypeMap[K] | undefined;
    getWatches(): WatchEntry[];
    getEffectiveWatchOptions(watchId: number): WatchOptions;

    onChange: Event<void>;
}

export const DefaulOptions: Readonly<WatchOptions> = Object.freeze({
    fileCheckInterval: 500,
    fileListInterval: 2000,
    ignorePattern: "(node_modules|.git)",
    encoding: undefined,
});

export interface VariableResolveContext {
    home: string;
    env: { [key: string]: string | undefined };
    workspaceFolder: string | undefined;
    // only meaningful on windows
    allowBackslashAsPathSeparator: boolean;
}

export function resolveVariables(
    pattern: string,
    { env, home, workspaceFolder, allowBackslashAsPathSeparator }: VariableResolveContext,
): string {
    const path = getPathImpl();
    if (!allowBackslashAsPathSeparator && path.sep === "\\") {
        // normalize path separators
        home = home.split(path.sep).join(path.posix.sep);
        if (workspaceFolder != null) {
            workspaceFolder = workspaceFolder.split(path.sep).join(path.posix.sep);
        }
    }
    pattern = pattern.replace(/^(~|\$HOME)(?=\W|$)/, home);
    return pattern.replace(/\$\{([^}]+)\}/g, (m, g1: string) => {
        switch (g1) {
            case "userHome":
                return home;
            case "workspaceFolder":
                if (workspaceFolder) {
                    return workspaceFolder;
                } else {
                    return m;
                }
            case "workspaceFolderBasename":
                if (workspaceFolder) {
                    return path.basename(workspaceFolder);
                } else {
                    return m;
                }
        }
        if (g1.startsWith("env:")) {
            const name = g1.slice("env:".length);
            const value = env[name];
            if (value) {
                // FIXME: normalize path separators on windows if allowBackslashAsPathSeparator is `false`?
                return value;
            }
        }
        return m;
    });
}
