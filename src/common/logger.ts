import { performance } from "perf_hooks";
import { LogLevel, type IConfigService } from "./config";
import { isDevEnv } from "./util";
import type { Disposable } from "./vscodeTypes";

function getLogLevel(config: IConfigService): LogLevel {
    const logLevelKey = config.get("logLevel");
    if (logLevelKey != null) {
        const logLevel = LogLevel[logLevelKey];
        if (logLevel != null) {
            return logLevel;
        }
    }
    if (isDevEnv()) {
        return LogLevel.debug;
    } else {
        return LogLevel.error;
    }
}

type LogParam = unknown;

export abstract class Logger implements Disposable {
    private readonly disposable: Disposable;

    constructor(config: IConfigService) {
        this.logLevel = getLogLevel(config);
        this.disposable = config.onChange(() => {
            this.logLevel = getLogLevel(config);
        });
    }
    private logLevel: LogLevel;
    private readonly _times: { [label: string]: number } = {};
    protected abstract log(level: string, x: LogParam): void;

    public trace(x: LogParam): void {
        if (this.logLevel > LogLevel.trace) {
            return;
        }
        this.log("[TRACE]", x);
    }

    public debug(x: LogParam): void {
        if (this.logLevel > LogLevel.debug) {
            return;
        }
        this.log("[DEBUG]", x);
    }

    public info(x: LogParam): void {
        if (this.logLevel > LogLevel.info) {
            return;
        }
        this.log("[INFO]", x);
    }

    public warn(x: LogParam): void {
        if (this.logLevel > LogLevel.warn) {
            return;
        }
        this.log("[WARN]", x);
    }

    public error(x: LogParam): void {
        if (this.logLevel > LogLevel.error) {
            return;
        }
        this.log("[ERROR]", x);
    }

    public timeStart(label: string): void {
        if (this.logLevel > LogLevel.trace) {
            return;
        }
        this._times[label] = performance.now();
    }
    public timeEnd(label: string): void {
        if (this.logLevel > LogLevel.trace) {
            return;
        }
        const t0 = this._times[label];
        if (t0) {
            const t1 = performance.now();
            delete this._times[label];
            const ms = (t1 - t0).toFixed(2);
            this.trace(`${label} ${ms} ms`);
        }
    }

    public dispose(): void {
        this.disposable.dispose();
    }
}
