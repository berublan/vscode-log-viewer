import * as vscode from "vscode";
import { registerInstance } from "./common/container";
import { setDevEnv } from "./common/util";
import { ConfigService } from "./vscode/config";
import { registerLogExplorer, type LogExplorerTestHandles } from "./vscode/logExplorer";
import { registerLogger } from "./vscode/logger";
import { registerLogWatchProvider } from "./vscode/logProvider";
import { registerStatusBarItems, type StatusBarItemsTestHandles } from "./vscode/statusBarItems";

export interface ExtensionTestHandles extends LogExplorerTestHandles, StatusBarItemsTestHandles {}

export function activate(context: vscode.ExtensionContext): ExtensionTestHandles | undefined {
    setDevEnv(context.extensionMode === vscode.ExtensionMode.Development);

    const subs = context.subscriptions;
    const configSvc = new ConfigService();
    subs.push(configSvc);
    registerInstance("config", configSvc);

    const logger = registerLogger(subs, configSvc);
    const logProvider = registerLogWatchProvider(subs, configSvc, logger);
    const statusBarItemsHandles = registerStatusBarItems(logProvider, subs, configSvc);
    const logExplorerHandles = registerLogExplorer(
        logProvider,
        subs,
        configSvc,
        logger,
        context.workspaceState,
    );

    if (context.extensionMode === vscode.ExtensionMode.Test) {
        return {
            ...logExplorerHandles,
            ...statusBarItemsHandles,
        };
    } else {
        return;
    }
}

export function deactivate(): void {}
