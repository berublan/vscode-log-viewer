import * as fs from "fs";
import * as vscode from "vscode";
import type { WatchOptions } from "../common/config";
import { getInstace } from "../common/container";
import { MultiPathMatcherWalker, SinglePathMatcherWalker, type FileInfo } from "../common/fsUtil";
import type { FsWalker } from "../common/fsWalker";
import type { Logger } from "../common/logger";
import { toPathMatcher } from "../common/mmUtil";
import { getWorkspaceDir, patternDescription } from "../common/util";
import type { WatchForUri } from "./logUri";

export interface GlobChange {
    readonly filename: string | undefined;
}

export type IGlobWatcherConstructor = new (options: WatchOptions, watch: WatchForUri) => IGlobWatcher;

export interface IGlobWatcher extends vscode.Disposable {
    readonly onChange: vscode.Event<GlobChange>;
    LastFile(): string | undefined;
    startWatch(): Promise<void>;
}

function getWalker(watch: WatchForUri, ignorePattern: string | undefined): FsWalker {
    const cwd = getWorkspaceDir(vscode.workspace.workspaceFolders, watch.workspaceName);

    if (Array.isArray(watch.pattern)) {
        const pathMatchers = watch.pattern.map(pattern =>
            toPathMatcher(pattern, {
                cwd: cwd,
                nameIgnorePattern: ignorePattern,
            }),
        );
        return new MultiPathMatcherWalker(pathMatchers);
    } else {
        const pathMatcher = toPathMatcher(watch.pattern, {
            cwd: cwd,
            nameIgnorePattern: ignorePattern,
        });
        return new SinglePathMatcherWalker(pathMatcher);
    }
}

class SimpleGlobWatcher implements IGlobWatcher {
    private readonly logger: Logger;
    private readonly walker: FsWalker;
    private readonly patternDescription: string;

    private fileTimer: NodeJS.Timer | undefined;
    private globTimer: NodeJS.Timer | undefined;

    private readonly _onChange = new vscode.EventEmitter<GlobChange>();

    private lastFile: FileInfo | undefined;

    public get onChange(): vscode.Event<GlobChange> {
        return this._onChange.event;
    }

    public LastFile(): string | undefined {
        return this.lastFile?.fullPath;
    }

    constructor(private readonly options: WatchOptions, readonly watch: WatchForUri) {
        this.logger = getInstace("logger");
        this.walker = getWalker(watch, this.options.ignorePattern);
        this.patternDescription = patternDescription(watch.pattern);
    }

    public async startWatch(): Promise<void> {
        await this.globTick();
        await this.fileTick();
    }

    private fileTick = async () => {
        if (this.lastFile) {
            try {
                const newStat = await fs.promises.stat(this.lastFile.fullPath);
                if (
                    newStat.mtime.getTime() !== this.lastFile.stats.mtime.getTime() ||
                    newStat.size !== this.lastFile.stats.size
                ) {
                    this._onChange.fire({
                        filename: this.lastFile.fullPath,
                    });
                }
            } catch (err) {
                // debug, because may have been removed
                this.logger.debug(err);
                this.lastFile = undefined;
                this._onChange.fire({
                    filename: undefined,
                });
            }
        }

        this.fileTimer = setTimeout(
            // eslint-disable-next-line @typescript-eslint/no-misused-promises
            this.fileTick,
            this.options.fileCheckInterval,
        );
    };

    private onError = (err: Error) => {
        // debug, because this can happen when trying to access
        // folders for which we don't have permissions,
        // and that should not be considered an error
        this.logger.debug(err);
    };

    private globTick = async () => {
        let maxMTime = 0;
        let maxFI: FileInfo | undefined;

        this.logger.timeStart(this.patternDescription);

        await this.walker.walk({
            onFile: fi => {
                const mt = fi.stats.mtime.getTime();
                if (mt > maxMTime) {
                    maxMTime = mt;
                    maxFI = fi;
                }
            },
            onError: this.onError,
        });

        this.logger.timeEnd(this.patternDescription);

        if (maxFI) {
            let newLastFile = false;
            if (this.lastFile) {
                if (maxFI.fullPath !== this.lastFile.fullPath) {
                    newLastFile = true;
                }
            } else {
                newLastFile = true;
            }
            if (newLastFile) {
                this.lastFile = maxFI;
                this._onChange.fire({
                    filename: maxFI.fullPath,
                });
            }
        } else {
            if (this.lastFile) {
                this.lastFile = undefined;
                this._onChange.fire({
                    filename: undefined,
                });
            }
        }

        this.globTimer = setTimeout(
            // eslint-disable-next-line @typescript-eslint/no-misused-promises
            this.globTick,
            this.options.fileListInterval,
        );
    };

    public dispose(): void {
        if (this.fileTimer) {
            clearTimeout(this.fileTimer);
        }
        if (this.globTimer) {
            clearTimeout(this.globTimer);
        }
    }
}

export const SimpleGlobWatcherConstructable: IGlobWatcherConstructor = SimpleGlobWatcher;
