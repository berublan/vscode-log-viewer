import * as fs from "fs";
import type { DecoderStream } from "iconv-lite";
import * as vscode from "vscode";
import type { WatchEntry } from "../common/config";
import { getInstace } from "../common/container";
import type { Logger } from "../common/logger";
import { assertNever, patternDescription } from "../common/util";
import type { ConfigService } from "./config";
import { SimpleGlobWatcherConstructable as GlobWatcher, type IGlobWatcher } from "./globWatcher";
import { fromLogUri, LogViewerSchema, toLogUri, type WatchForUri } from "./logUri";

function getChunkSize(configSvc: ConfigService) {
    let chunkSize = configSvc.get("chunkSizeKb");
    if (!chunkSize || chunkSize <= 0) {
        chunkSize = 64;
    }
    return chunkSize * 1024;
}

const utf8Encoder = new TextEncoder();

async function lastChunk(
    file: string,
    decoder: DecoderStream | undefined,
    offset: number | undefined,
    configSvc: ConfigService,
): Promise<Uint8Array> {
    const chunkSize = getChunkSize(configSvc);

    const buffer = Buffer.alloc(2 * chunkSize);

    if (!offset || offset < 0) {
        offset = 0;
    }
    const fd = await fs.promises.open(file, "r");
    try {
        const stat = await fd.stat();
        const partSize = stat.size - offset;
        if (partSize <= 0) {
            return new Uint8Array();
        }
        let res;
        if (partSize > chunkSize) {
            const lastChunkSize = partSize % chunkSize;
            const readSize = chunkSize + lastChunkSize;
            res = await fd.read(buffer, 0, readSize, stat.size - readSize);
        } else {
            res = await fd.read(buffer, 0, partSize, offset);
        }
        const buff = res.buffer.slice(0, res.bytesRead);
        if (decoder != null) {
            const decodeRes = decoder.write(buff);
            const decodeTrail = decoder.end();
            return utf8Encoder.encode(decodeTrail ? decodeRes + decodeTrail : decodeRes);
        } else {
            return new Uint8Array(buff);
        }
    } finally {
        await fd.close();
    }
}

const _decoders: { [encoding: string]: DecoderStream | undefined } = {};
function getDecoder(encoding: string | undefined | null): DecoderStream | undefined {
    if (!encoding) {
        return;
    }
    let decoder = _decoders[encoding];
    if (decoder) {
        // clear internal buffer
        decoder.end();
        return decoder;
    }
    try {
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        decoder = (require("iconv-lite") as typeof import("iconv-lite")).getDecoder(encoding);
        _decoders[encoding] = decoder;
        return decoder;
    } catch (error) {
        getInstace("logger").error(error);
        return;
    }
}

function uint8ArrayEquals(a: Uint8Array | undefined, b: Uint8Array | undefined): boolean {
    if (a == null || b == null) {
        return (a == null) === (b == null);
    }
    if (a.length !== b.length) {
        return false;
    }
    for (let i = 0; i < a.length; i++) {
        if (a[i] !== b[i]) {
            return false;
        }
    }
    return true;
}

interface WatchStateInternal {
    // undefined when stopped
    watcher: IGlobWatcher | undefined;
    readonly decoder: DecoderStream | undefined;
    lastFileName: string | undefined;
    offset: number | undefined;
    bytes: Uint8Array | undefined;
    createdOn: Date;
    lastChangedOn: Date;
}

export interface WatchState {
    readonly running: boolean;
    readonly uri: vscode.Uri;
    readonly lastFileName: string | undefined;
    readonly bytes: Uint8Array | undefined;
    readonly createdOn: Date;
    readonly lastChangedOn: Date;
}

function createWatchState(uri: vscode.Uri, w: WatchStateInternal): WatchState {
    return {
        running: w.watcher != null,
        bytes: w.bytes,
        createdOn: w.createdOn,
        lastChangedOn: w.lastChangedOn,
        lastFileName: w.lastFileName,
        uri,
    };
}

export const enum EventType {
    Start,
    ContentChange,
    FileChange,
    Stop,
}

export type WatchEvent = {
    readonly type: EventType;
    readonly uri: vscode.Uri;
};

export class LogWatchProvider implements vscode.Disposable {
    private readonly _onChange = new vscode.EventEmitter<WatchEvent>();
    private readonly _subs: vscode.Disposable[] = [this._onChange];
    private readonly _watchedUris: Map<string, WatchStateInternal> = new Map<string, WatchStateInternal>();

    constructor(private readonly configSvc: ConfigService, private readonly logger: Logger) {
        this._subs.push(
            configSvc.onChange(() => {
                this.checkForOrphanWatches();
            }),
        );
    }

    public isWatching(uri: vscode.Uri): boolean {
        const state = this._watchedUris.get(uri.toString());
        return state != null && state.watcher != null;
    }

    public get(uri: vscode.Uri): WatchState | undefined {
        const state = this._watchedUris.get(uri.toString());
        if (state == null) {
            return;
        }
        return createWatchState(uri, state);
    }

    public async clearContents(uri: vscode.Uri): Promise<void> {
        const state = this._watchedUris.get(uri.toString());
        if (state != null && state.watcher != null && state.lastFileName != null) {
            const stat = await fs.promises.stat(state.lastFileName);
            state.offset = stat.size;
            await this.checkChange(uri, state, state.lastFileName);
        }
    }

    public async restoreContents(uri: vscode.Uri): Promise<void> {
        const state = this._watchedUris.get(uri.toString());
        if (state != null && state.watcher != null && state.lastFileName != null) {
            state.offset = undefined;
            await this.checkChange(uri, state, state.lastFileName);
        }
    }

    public async startWatch(uri: vscode.Uri, startIfStopped: boolean): Promise<WatchState> {
        const uriStr = uri.toString();
        const foundState = this._watchedUris.get(uriStr);
        if (foundState != null && (!startIfStopped || foundState.watcher != null)) {
            return createWatchState(uri, foundState);
        }
        const w = fromLogUri(uri);
        const options = this.configSvc.getEffectiveWatchOptions(w.id);
        const now = new Date();
        const newState = {
            watcher: new GlobWatcher(options, w),
            decoder: getDecoder(options.encoding),
            createdOn: now,
            lastChangedOn: now,
            lastFileName: undefined,
            offset: undefined,
            bytes: undefined,
        } satisfies WatchStateInternal;
        newState.watcher.onChange(e => {
            void this.checkChange(uri, newState, e.filename);
        });
        this._watchedUris.set(uriStr, newState);

        this.logger.info(`Starting watch: "${watchDescription(w)}"`);
        this._onChange.fire({ uri, type: EventType.Start });
        await newState.watcher.startWatch();
        return createWatchState(uri, newState);
    }

    private async checkChange(
        uri: vscode.Uri,
        _state: WatchStateInternal,
        filename: string | undefined,
    ): Promise<boolean> {
        // otherwise assigning to state below triggers require-atomic-updates eslint rule
        const state = _state;

        const newBytes: Uint8Array | undefined =
            filename == null
                ? undefined
                : await lastChunk(filename, state.decoder, state.offset, this.configSvc);

        // check if filename changed
        let changeType: EventType.ContentChange | EventType.FileChange | null = null;
        if (state.lastFileName !== filename) {
            state.lastFileName = filename;
            state.offset = undefined;
            changeType = EventType.FileChange;
        }

        // check if content changed
        if (!uint8ArrayEquals(state.bytes, newBytes)) {
            state.bytes = newBytes;
            changeType ??= EventType.ContentChange;
        }

        if (changeType != null) {
            state.lastChangedOn = new Date();
            this.logger.debug(() => {
                const w = fromLogUri(uri);
                let msg = `Change for "${watchDescription(w)}"`;
                if (filename) {
                    msg += ` on ${filename}`;
                }
                return msg;
            });
            this._onChange.fire({
                uri: uri,
                type: changeType,
            });
        }

        return changeType != null;
    }

    get onChange(): vscode.Event<WatchEvent> {
        return this._onChange.event;
    }

    private didStopWatch(uri: vscode.Uri) {
        this._onChange.fire({ uri, type: EventType.Stop });
        this.logger.info(() => {
            const w = fromLogUri(uri);
            return `Stopping watch: "${watchDescription(w)}"`;
        });
    }

    public stopWatch(uri: vscode.Uri): void {
        const uriStr = uri.toString();
        const state = this._watchedUris.get(uriStr);
        if (state != null && state.watcher != null) {
            state.watcher.dispose();
            state.watcher = undefined;
            this.didStopWatch(uri);
        }
    }

    public stopAllWatches(): void {
        for (const [uriStr, state] of this._watchedUris.entries()) {
            if (state.watcher != null) {
                state.watcher.dispose();
                state.watcher = undefined;
                this.didStopWatch(vscode.Uri.parse(uriStr));
            }
        }
    }

    private checkForOrphanWatches() {
        // remove watches whose config has been changed or removed
        const newWatchUriStrs = new Set<string>();
        function collectWatches(ws: readonly WatchEntry[]) {
            for (const w of ws) {
                switch (w.kind) {
                    case "watch": {
                        const uri = toLogUri(w);
                        newWatchUriStrs.add(uri.toString());
                        break;
                    }
                    case "group": {
                        collectWatches(w.watches);
                        break;
                    }
                    default:
                        assertNever(w);
                }
            }
        }
        collectWatches(this.configSvc.getWatches());

        for (const [uriStr, state] of this._watchedUris.entries()) {
            if (!newWatchUriStrs.has(uriStr)) {
                this._watchedUris.delete(uriStr);
                if (state.watcher != null) {
                    state.watcher.dispose();
                    state.watcher = undefined;
                    this.didStopWatch(vscode.Uri.parse(uriStr));
                }
            }
        }
    }

    public dispose(): void {
        this.stopAllWatches();
        for (const sub of this._subs) {
            sub.dispose();
        }
        this._watchedUris.clear();
    }
}

function watchDescription(w: WatchForUri): string {
    return w.title ?? patternDescription(w.pattern);
}

const NoMatchingFileMsgBytes: Uint8Array = new TextEncoder().encode("no matching file found");
const WatchNotRunningMsgBytes: Uint8Array = new TextEncoder().encode("watch not running");

const EmptyDisposable: vscode.Disposable = { dispose: () => {} };

class LogViewerFileSystemProvider implements vscode.FileSystemProvider, vscode.Disposable {
    private readonly _onDidChangeFile = new vscode.EventEmitter<vscode.FileChangeEvent[]>();

    private readonly _subs: vscode.Disposable[] = [this._onDidChangeFile];

    constructor(private readonly logger: Logger, private readonly logProvider: LogWatchProvider) {
        this._subs.push(
            logProvider.onChange(e => {
                if (e.type !== EventType.Stop) {
                    this._onDidChangeFile.fire([{ type: vscode.FileChangeType.Changed, uri: e.uri }]);
                }
            }),
        );
    }

    get onDidChangeFile(): vscode.Event<vscode.FileChangeEvent[]> {
        return this._onDidChangeFile.event;
    }

    watch(_uri: vscode.Uri, _options: { recursive: boolean; excludes: string[] }): vscode.Disposable {
        return EmptyDisposable;
    }

    async stat(uri: vscode.Uri): Promise<vscode.FileStat> {
        const state = await this.logProvider.startWatch(uri, false);
        if (state == null) {
            throw vscode.FileSystemError.FileNotFound(uri);
        }
        return {
            type: vscode.FileType.File,
            ctime: state.createdOn.getTime(),
            mtime: state.lastChangedOn.getTime(),
            size: (state.bytes ?? NoMatchingFileMsgBytes).length,
        };
    }

    readDirectory(_uri: vscode.Uri): [string, vscode.FileType][] {
        throw new Error("Method not supported.");
    }

    createDirectory(_uri: vscode.Uri): void {
        throw new Error("Method not supported.");
    }

    async readFile(uri: vscode.Uri): Promise<Uint8Array> {
        const state = await this.logProvider.startWatch(uri, false);
        if (state == null) {
            return WatchNotRunningMsgBytes;
        }
        return state.bytes ?? NoMatchingFileMsgBytes;
    }

    writeFile(
        _uri: vscode.Uri,
        _content: Uint8Array,
        _options: { create: boolean; overwrite: boolean },
    ): void | Thenable<void> {
        throw new Error("Method not supported.");
    }

    delete(_uri: vscode.Uri, _options: { recursive: boolean }): void | Thenable<void> {
        throw new Error("Method not supported.");
    }

    rename(
        _oldUri: vscode.Uri,
        _newUri: vscode.Uri,
        _options: { overwrite: boolean },
    ): void | Thenable<void> {
        throw new Error("Method not supported.");
    }

    dispose(): void {
        for (const sub of this._subs) {
            sub.dispose();
        }
    }
}

export function registerLogWatchProvider(
    subs: vscode.Disposable[],
    configSvc: ConfigService,
    logger: Logger,
): LogWatchProvider {
    const logProvider = new LogWatchProvider(configSvc, logger);

    subs.push(logProvider);

    const logViewerFileSystemProvider = new LogViewerFileSystemProvider(logger, logProvider);

    subs.push(logViewerFileSystemProvider);
    subs.push(
        vscode.workspace.registerFileSystemProvider(LogViewerSchema, logViewerFileSystemProvider, {
            isReadonly: true,
        }),
    );

    subs.push(
        vscode.workspace.onDidCloseTextDocument(doc => {
            if (doc.uri.scheme === LogViewerSchema) {
                logProvider.stopWatch(doc.uri);
            }
        }),
    );

    return logProvider;
}
