import * as vscode from "vscode";
import { registerInstance } from "../common/container";
import { Logger } from "../common/logger";
import type { ConfigService } from "./config";

const showExtensionLogsCmd = "logviewer.showExtensionLogs";

function toStr(x: unknown): string {
    function inner(x: unknown): string {
        if (typeof x === "string") {
            return x;
        } else {
            return JSON.stringify(x, undefined, "\t");
        }
    }

    if (typeof x === "function" && x.length === 0) {
        return toStr(x());
    } else {
        return inner(x);
    }
}

class OutputChannelLogger extends Logger implements vscode.Disposable {
    private _outputChannel: vscode.OutputChannel | undefined;
    private get outputChannel(): vscode.OutputChannel {
        if (!this._outputChannel) {
            this._outputChannel = vscode.window.createOutputChannel("log-viewer");
        }
        return this._outputChannel;
    }
    protected log(level: string, x: unknown): void {
        const str = level + " " + toStr(x);
        this.outputChannel.appendLine(str);
    }

    public override dispose(): void {
        if (this._outputChannel) {
            this._outputChannel.dispose();
        }
        super.dispose();
    }

    public show(): void {
        this.outputChannel.show();
    }
}

export function registerLogger(subs: vscode.Disposable[], configSvc: ConfigService): Logger {
    const logger = new OutputChannelLogger(configSvc);
    subs.push(logger);
    registerInstance("logger", logger);

    subs.push(
        vscode.commands.registerCommand(showExtensionLogsCmd, () => {
            logger.show();
        }),
    );

    return logger;
}
