import * as os from "os";
import * as process from "process";
import * as vscode from "vscode";
import {
    DefaulOptions,
    resolveVariables,
    VariableResolveContext,
    type ConfigTypeMap,
    type IConfigService,
    type Watch,
    type WatchEntry,
    type WatchOptions,
} from "../common/config";

interface ConfigGroup {
    readonly groupName: string;
    readonly watches: ReadonlyArray<ConfigEntry>;
}

interface ConfigWatch {
    readonly title: string | undefined;
    readonly pattern: string | string[];
    readonly workspaceName: string | undefined;
    readonly options: Partial<WatchOptions> | undefined;
}

type ConfigEntry = string | ConfigWatch | ConfigGroup;

interface InternalConfigTypeMap extends ConfigTypeMap {
    watch: ConfigEntry[] | undefined;
}

const configurationSection = "logViewer";

export class ConfigService implements vscode.Disposable, IConfigService {
    private readonly _onChange = new vscode.EventEmitter<void>();
    private config!: vscode.WorkspaceConfiguration & InternalConfigTypeMap;
    private readonly watches: WatchEntry[] = [];
    private readonly watchesById = new Map<number, Watch>();
    private seqId = 0;

    constructor() {
        this.load();
        vscode.workspace.onDidChangeConfiguration(e => {
            if (e.affectsConfiguration(configurationSection)) {
                this.load();
                this._onChange.fire();
            }
        });
    }

    private nextId() {
        return this.seqId++;
    }

    private readonly toWatchEntry = (configEntry: ConfigEntry, ctx: VariableResolveContext): WatchEntry => {
        if (typeof configEntry === "string") {
            const watch: Watch = {
                kind: "watch",
                id: this.nextId(),
                options: undefined,
                pattern: resolveVariables(configEntry, ctx),
                title: configEntry,
                workspaceName: undefined,
            };
            this.watchesById.set(watch.id, watch);
            return watch;
        } else if ("groupName" in configEntry) {
            return {
                kind: "group",
                groupName: configEntry.groupName,
                watches: configEntry.watches.map(x => this.toWatchEntry(x, ctx)),
            };
        } else {
            const watch: Watch = {
                kind: "watch",
                id: this.nextId(),
                ...configEntry,
                pattern: Array.isArray(configEntry.pattern)
                    ? configEntry.pattern.map(x => resolveVariables(x, ctx))
                    : resolveVariables(configEntry.pattern, ctx),
            };
            this.watchesById.set(watch.id, watch);
            return watch;
        }
    };

    get onChange(): vscode.Event<void> {
        return this._onChange.event;
    }

    private load() {
        this.watches.splice(0);
        this.watchesById.clear();
        this.seqId = 0;
        this.config = vscode.workspace.getConfiguration(configurationSection) as ConfigService["config"];
        const configWatches = this.config.watch;
        if (configWatches) {
            const ctx: VariableResolveContext = {
                env: process.env,
                home: os.homedir(),
                workspaceFolder: vscode.workspace.workspaceFolders?.[0]?.uri.fsPath,
                allowBackslashAsPathSeparator: this.config.windows.allowBackslashAsPathSeparator,
            };
            for (const w of configWatches) {
                this.watches.push(this.toWatchEntry(w, ctx));
            }
        }
    }

    public get<K extends keyof ConfigTypeMap>(key: K): ConfigTypeMap[K] | undefined {
        return this.config.get(key);
    }

    public getWatches(): WatchEntry[] {
        return this.watches;
    }

    public getEffectiveWatchOptions(watchId: number): WatchOptions {
        // copy
        const resultOpts = Object.assign({}, DefaulOptions);

        const globalOpts = this.config.options;

        if (globalOpts) {
            Object.assign(resultOpts, globalOpts);
        }

        const watch = this.watchesById.get(watchId);
        if (watch?.options) {
            Object.assign(resultOpts, watch.options);
        }

        return resultOpts;
    }

    public dispose(): void {
        this._onChange.dispose();
    }
}
