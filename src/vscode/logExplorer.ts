import * as vscode from "vscode";
import type { WatchEntry } from "../common/config";
import type { Logger } from "../common/logger";
import type { ConfigService } from "./config";
import { EventType, type LogWatchProvider } from "./logProvider";
import { LogViewerSchema, toLogUri } from "./logUri";

export const openLogResourceCmd = "logviewer.openLogResource";
export const unwatchCmd = "logviewer.unwatchLogResource";
const unwatchAllCmd = "logviewer.unwatchAll";

interface GroupItem {
    readonly id: string;
    readonly kind: "group";
    readonly parent: GroupItem | undefined;
    readonly name: string;
    readonly items: Item[];
}

interface WatchItem {
    readonly kind: "watch";
    readonly parent: GroupItem | undefined;
    readonly title?: string;
    readonly pattern: string | string[];
    readonly uri: vscode.Uri;
}

type Item = GroupItem | WatchItem;

function toItem(entry: WatchEntry, parent: GroupItem | undefined, accIds: Set<string>): Item {
    if (entry.kind === "group") {
        const items: Item[] = [];
        const groupItem: GroupItem = {
            id: parent == null ? entry.groupName : `${parent.id}.${entry.groupName}`,
            kind: "group",
            parent,
            name: entry.groupName,
            items,
        };
        accIds.add(groupItem.id);
        for (const x of entry.watches) {
            items.push(toItem(x, groupItem, accIds));
        }
        return groupItem;
    } else {
        return {
            kind: "watch",
            parent,
            pattern: entry.pattern,
            title: entry.title,
            uri: toLogUri(entry),
        };
    }
}

class LogExplorer implements vscode.TreeDataProvider<Item>, vscode.Disposable {
    private readonly disposable: vscode.Disposable;

    public static readonly ViewId: string = "logExplorer";

    private readonly _onDidChange: vscode.EventEmitter<undefined>;

    private rootItems: Item[] = [];

    constructor(
        private readonly logProvider: LogWatchProvider,
        private readonly configSvc: ConfigService,
        private readonly expandedIds: Set<string>,
    ) {
        this._onDidChange = new vscode.EventEmitter();

        this.disposable = vscode.Disposable.from(
            this._onDidChange,
            configSvc.onChange(() => {
                this.reload();
            }),
            logProvider.onChange(e => {
                if (e.type === EventType.Start || e.type === EventType.Stop) {
                    this.reload();
                }
            }),
        );

        this.updateItems();
    }

    private updateItems() {
        const validIds = new Set<string>();
        this.rootItems = this.configSvc.getWatches().map(x => toItem(x, undefined, validIds));
        for (const id of this.expandedIds.values()) {
            if (!validIds.has(id)) {
                this.expandedIds.delete(id);
            }
        }
    }

    public get onDidChangeTreeData() {
        return this._onDidChange.event;
    }

    private reload() {
        this.updateItems();
        this._onDidChange.fire(undefined);
    }

    public getTreeItem(element: Item) {
        if (element.kind === "group") {
            const item = new vscode.TreeItem(
                element.name,
                this.expandedIds.has(element.id)
                    ? vscode.TreeItemCollapsibleState.Expanded
                    : vscode.TreeItemCollapsibleState.Collapsed,
            );
            item.id = element.id;
            return item;
        } else {
            const watching = this.logProvider.isWatching(element.uri);
            let name: string;
            if (element.title) {
                name = element.title;
            } else if (Array.isArray(element.pattern)) {
                name = element.pattern.join(",");
            } else {
                name = element.pattern;
            }
            const item = new vscode.TreeItem(name);
            if (watching) {
                item.iconPath = new vscode.ThemeIcon("eye");
                item.contextValue = "watching";
            } else {
                item.iconPath = undefined; //vscode.ThemeIcon.File;
                item.contextValue = undefined;
            }
            item.command = {
                command: openLogResourceCmd,
                arguments: [element.uri],
                title: name,
                tooltip: name,
            };
            return item;
        }
    }

    public getParent(element: Item): GroupItem | undefined {
        return element.parent;
    }

    public getChildren(element?: Item): Item[] | undefined {
        if (element === undefined) {
            return this.rootItems;
        } else if (element.kind === "group") {
            return element.items;
        }
        return;
    }

    public dispose() {
        this.disposable.dispose();
    }
}

export interface LogExplorerTestHandles {
    treeDataProvider: vscode.TreeDataProvider<Item>;
    treeView: vscode.TreeView<Item>;
}

export function registerLogExplorer(
    logProvider: LogWatchProvider,
    subs: vscode.Disposable[],
    configSvc: ConfigService,
    logger: Logger,
    workspaceState: vscode.Memento,
): LogExplorerTestHandles {
    const expandedIdsStateKey = LogExplorer.ViewId + ".expandedIds";
    const expandedIds = new Set<string>(
        (() => {
            const json = workspaceState.get<string>(expandedIdsStateKey);
            if (json != null) {
                try {
                    return JSON.parse(json) as string[];
                } catch (error) {
                    logger.error(`${expandedIdsStateKey}: failed to deserialize "${json}"`);
                }
            }
            return null;
        })(),
    );
    const logExplorer = new LogExplorer(logProvider, configSvc, expandedIds);
    subs.push(logExplorer);

    const treeView = vscode.window.createTreeView(LogExplorer.ViewId, {
        treeDataProvider: logExplorer,
        showCollapseAll: true,
    });

    subs.push(treeView);

    let updateStateHandle: NodeJS.Timeout | undefined;
    const queueStateUpdate = () => {
        clearTimeout(updateStateHandle);
        updateStateHandle = setTimeout(() => {
            const json = JSON.stringify(Array.from(expandedIds.values()));
            void workspaceState.update(expandedIdsStateKey, json);
        }, 250);
    };
    treeView.onDidCollapseElement(e => {
        if (e.element.kind === "group") {
            expandedIds.delete(e.element.id);
            queueStateUpdate();
        }
    });
    treeView.onDidExpandElement(e => {
        if (e.element.kind === "group") {
            expandedIds.add(e.element.id);
            queueStateUpdate();
        }
    });

    const descUnknown = (x: unknown): string => {
        if (typeof x === "string") {
            return x;
        }
        const s = String(x);
        if (s !== "[object Object]") {
            return s;
        }
        try {
            return JSON.stringify(s);
        } catch {
            return s;
        }
    };

    subs.push(
        vscode.commands.registerCommand(openLogResourceCmd, async (logUri: unknown) => {
            let uri: vscode.Uri;
            if (logUri instanceof vscode.Uri) {
                uri = logUri;
            } else if (typeof logUri === "string") {
                uri = vscode.Uri.parse(logUri);
            } else {
                logger.error(`unexpected argument type for "${openLogResourceCmd}": ${descUnknown(logUri)}`);
                return;
            }
            await logProvider.startWatch(uri, true);
            const doc = await vscode.workspace.openTextDocument(uri);
            await vscode.window.showTextDocument(doc, { preview: false });
        }),
    );

    subs.push(
        vscode.commands.registerCommand(unwatchCmd, (x: unknown) => {
            let uri: vscode.Uri;
            if (x == null) {
                const editor = vscode.window.activeTextEditor;
                if (editor == null || editor.document.uri.scheme !== LogViewerSchema) {
                    return;
                }
                uri = editor.document.uri;
            } else if (typeof x === "string") {
                uri = vscode.Uri.parse(x);
            } else if ((x as { uri: vscode.Uri }).uri instanceof vscode.Uri) {
                uri = (x as { uri: vscode.Uri }).uri;
            } else {
                logger.error(`"${unwatchCmd}": unexpected argument ${descUnknown(x)}`);
                return;
            }
            logProvider.stopWatch(uri);
        }),
    );

    subs.push(
        vscode.commands.registerCommand(unwatchAllCmd, () => {
            logProvider.stopAllWatches();
        }),
    );

    return {
        treeView: treeView,
        treeDataProvider: logExplorer,
    };
}
